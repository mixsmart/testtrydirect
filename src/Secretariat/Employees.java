package Secretariat;

import org.openqa.selenium.WebDriver;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class Employees extends ParlamentCategory{
	
	public Employees(){
		super.key = "employees";
	}
	
	
	protected void addingCore(WebDriver driver){
		firstName = VAR.getRandomText();
		Toolkit.textField(driver, firstName, "input id='"+Key+"FirstName'");
		 secondName = VAR.getRandomText();
		Toolkit.textField(driver, secondName, "input id='"+Key+"LastName'");
		Toolkit.activate(driver, Key+"Active_chosen", 0);

	}
	  

}
