package Secretariat;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class Subdivisions extends ParlamentCategory{
	public Subdivisions(){
		super.key = "subdivisions";
	}
	
	
	protected void addingCore(WebDriver driver){
		firstName = VAR.getRandomText();
		Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
		WebElement iframe = driver.findElement(By.xpath("//iframe[@frameborder='0']"));
		driver.switchTo().frame(iframe);
		Toolkit.textField(driver.findElement(By.tagName("body")), VAR.getRandomText(), driver);
		driver.switchTo().defaultContent();
		Toolkit.activate(driver, Key+"Active_chosen", 0);

	}

}
