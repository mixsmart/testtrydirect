package Presa;

import NeoTesting.ParlamentCategory;

public class Pages extends ParlamentCategory{

	public Pages(){
		super.key = "pages";
	}
	
	public Pages(String login, String pwd){
		super.key = "pages";
		super.login = login;
		super.pwd = pwd;
	}
	
}
