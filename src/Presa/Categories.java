package Presa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class Categories extends ParlamentCategory{

	public Categories(){
		super.key = "categories";
	}
	
	public Categories(String login, String pwd){
		super.key = "categories";
		super.login = login;
		super.pwd = pwd;
	}
	
	 protected void keyPrepare(){
		Key = "Category";
		tool.k(Key);
		
	}
	
	

	  
}
