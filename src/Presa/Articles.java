package Presa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class Articles extends ParlamentCategory{

	public Articles(){
		super.key = "articles";
	}
	
	public Articles(String login, String pwd){
		super.key = "articles";
		super.login = login;
		super.pwd = pwd;
	}
	
	
	
	
	
	  protected void addingCore(WebDriver driver){
			
		firstName = VAR.getRandomText();
		
		Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
		
		WebElement iframe = driver.findElements(By.xpath("//iframe[@frameborder='0']")).get(1);
		driver.switchTo().frame(iframe);
		Actions actions = new Actions(driver);
		actions.sendKeys(driver.findElement(By.tagName("body")), VAR.getRandomText()).perform();

		driver.switchTo().defaultContent();
		
		
		Toolkit.dropDown(driver, "div id='Category_chosen'", "div id='Category_chosen'", "li data-option-array-index='2'");
		
		sleep(1);
		
		Toolkit.activate(driver, Key+"Active_chosen", 0);
		

	}
	
	
	  
	  
	  
}
