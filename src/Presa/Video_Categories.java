package Presa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class Video_Categories extends ParlamentCategory{

	
	public Video_Categories(){
		super.key = "video_categories";
	}
	
	public Video_Categories(String login, String pwd){
		super.key = "video_categories";
		super.login = login;
		super.pwd = pwd;
	}
	
	 protected void keyPrepare(){
		Key = "VideoCategory";
		tool.k(Key);
		
	}
	 
	  protected void addingCore(WebDriver driver){
			
		firstName = VAR.getRandomText();
		
		Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
		
		
		Toolkit.activate(driver, Key+"Active_chosen", 0);

	}
}
