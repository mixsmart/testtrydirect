package Sedinte;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class Events extends ParlamentCategory{

	public Events(){
		super.key = "events";
		super.tdLocator = 1;
	}
	
	
	protected void addingCore(WebDriver driver){
		firstName = VAR.getRandomText();
		Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
		WebElement iframe = driver.findElements(By.xpath("//iframe[@frameborder='0']")).get(1);
		
		driver.switchTo().frame(iframe);
		Toolkit.textField(driver.findElement(By.tagName("body")), VAR.getRandomText(), driver);
		driver.switchTo().defaultContent();
		
		
		
		
		Toolkit.dropDown(driver, "div id='CalendarCalendar_chosen'", "div id='CalendarCalendar_chosen'", "li data-option-array-index='1'");
		
		
		
		
		
		Toolkit.activate(driver, Key+"Active_chosen", 0);
	}
	  
	  

	
}
