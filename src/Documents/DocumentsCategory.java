package Documents;

import org.openqa.selenium.WebDriver;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class DocumentsCategory extends ParlamentCategory{

	public DocumentsCategory(){
		super.key = "document_categories";
	}
	
	public DocumentsCategory(String login, String pwd){
		super.key = "document_categories";
		super.login = login;
		super.pwd = pwd;
	}
	
	  protected void addingCore(WebDriver driver){
			
		firstName = VAR.getRandomText();
		Toolkit.textField(driver, firstName, "input id='"+"DocumentCategory"+"TitleRum'");
	}
}
