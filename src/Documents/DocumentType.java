package Documents;

import org.openqa.selenium.WebDriver;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class DocumentType extends ParlamentCategory{

	public DocumentType(){
		super.key = "document_types";
		super.tdLocator = 1;
	}
	
	public DocumentType(String login, String pwd){
		super.key = "document_types";
		super.login = login;
		super.pwd = pwd;
		super.tdLocator = 1;
	}
	
	  protected void addingCore(WebDriver driver){
			
		firstName = VAR.getRandomText();
		Toolkit.textField(driver, firstName, "input id='"+"DocumentType"+"TitleRum'");
	}
}
