package Documents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class Document extends ParlamentCategory{

	public Document(){
		super.key = "documents";
		super.tdLocator = 3;
	}
	
	public Document(String login, String pwd){
		super.key = "documents";
		super.login = login;
		super.pwd = pwd;
		super.tdLocator = 3;
	}
	
	  protected void addingCore(WebDriver driver){
			
		firstName = VAR.getRandomText();
		
		Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
		
		Toolkit.dropDown(driver, "div id='DocumentCategory_chosen'", "div id='DocumentCategory_chosen'", "li data-option-array-index='1'");
		
		WebElement iframe = driver.findElements(By.xpath("//iframe[@frameborder='0']")).get(1);
		
		driver.switchTo().frame(iframe);
		Toolkit.textField(driver.findElement(By.tagName("body")), VAR.getRandomText(), driver);
		driver.switchTo().defaultContent();
		
		Toolkit.activate(driver, Key+"Active_chosen", 0);
		

	}
}
