import java.util.ArrayList;

import Documents.Document;
import Documents.DocumentType;
import Documents.DocumentsCategory;
import NeoTesting.Fractiuni;
import NeoTesting.Conferences;
import NeoTesting.Implications;
import NeoTesting.ParlamentCategory;
import NeoTesting.Transcripts;
import NeoTesting.UserGroups;
import NeoTesting.Visits;
import NeoTesting.workgroups;
import NeoTesting.Comisii;
import NeoTesting.Cooperations;
import NeoTesting.Delegations;
import NeoTesting.Legislature;
import NeoTesting.CoreDeput;
import OptimumWeb.MainToolkit;
import Presa.Archives;
import Presa.Articles;
import Presa.Audio;
import Presa.Categories;
import Presa.Pages;
import Presa.Video_Categories;
import Presa.Videos;
import Proiecte_Legislative.AdvesoryEntities;
import Proiecte_Legislative.ProjectCategories;
import Proiecte_Legislative.ProjectProcedures;
import Proiecte_Legislative.ProjectStages;
import Proiecte_Legislative.Projects;
import Proiecte_Legislative.PublicAuthorities;
import Secretariat.Employees;
import Secretariat.Groups;
import Secretariat.Subdivisions;
import Sedinte.Calendar;
import Sedinte.Events;
import SpecialTests.Fractions;
public class Entry {
static MainToolkit tool = new MainToolkit();
static int numThreads = 0;
static String report= "";

static int neededThreads = 4;
public static void main(String... args){
	//ExperimentBitbucketTestModifyRecord	
		
		
		String login = "superadmin";
//		String login  = "testadmin";
		
		String pswd = "superman";
//		String pswd = "testadmin";
		
		
	ArrayList<ParlamentCategory> allData = new ArrayList<>();
	ParlamentCategory data = new Fractiuni();
//	allData.add(data);
//
//	data = new workgroups();
//	allData.add(data);
//
//	data = new Comisii();
//	allData.add(data);
//
//	data = new Cooperations();
//	allData.add(data);
//
//	data = new Delegations();
//	allData.add(data);
//	
//	data = new Conferences();
//	allData.add(data);
//
//	data = new Implications();
//	allData.add(data);
//
//	data = new Transcripts();
//	allData.add(data);
//
//	data = new UserGroups();
//	allData.add(data);
//
//	data = new Visits();
//	allData.add(data);
//
//	data = new Legislature();
//	allData.add(data);
//
//	data = new CoreDeput();
//	allData.add(data);
//	
//	//PRESA
//	
//	data = new Articles();
//	allData.add(data);
//	
//	data = new Categories();
//	allData.add(data);
//	
//	
//	data = new Pages();
//	allData.add(data);
//	
//	data = new Audio();
//	allData.add(data);
//	
//	data = new Videos();
//	allData.add(data);
//	
//	data = new Video_Categories();
//	allData.add(data);
//	
//	data = new Archives();
//	allData.add(data);
//	
//	//PROIECTE LEGISLATIVE
//	
//	data = new Projects();
//	allData.add(data);
//	
//	data = new ProjectCategories();
//	allData.add(data);
//	
//	data = new ProjectProcedures();
//	allData.add(data);
//	
//	data = new ProjectStages();
//	allData.add(data);
//	
//	data = new PublicAuthorities();
//	allData.add(data);
//
//	data = new AdvesoryEntities();
//	allData.add(data);
//
//	data = new Employees();
//	allData.add(data);
//	
//	data = new Groups();
//	allData.add(data);
//	
//	data = new Subdivisions();
//	allData.add(data);
//	
//	
//	data = new Events();
//	allData.add(data);
//	
//	data = new Calendar();
//	allData.add(data);
//	
//	data = new Document();
//	allData.add(data);
//	
//	data = new DocumentsCategory();
//	allData.add(data);
//	
//	data = new DocumentType();
//	allData.add(data);
	
	
	data = new Fractions();
	allData.add(data);

	for(ParlamentCategory curent:allData){
//		tool.sleepS(1);
	
		if(numThreads>=neededThreads){
			while(numThreads>=neededThreads){
				tool.sleep(5);
			}
		}
		
		new Entry.Multi(curent, login, pswd).start();
		
		if(numThreads>=neededThreads){
			while(numThreads>=neededThreads){
				tool.sleep(5);
			}
		}
		
	}
	
	while(numThreads>0){
		tool.sleep(5);
	}
	
	tool.fastWrite(report, "./report");
		
		
		
	}
	
	
	 static class Multi extends Thread{
		ParlamentCategory data; 
		String login = "";
		String pswd = "";
		 
		public Multi(ParlamentCategory data, String login, String pswd){
			plus();
			this.data = data;
			this.login = login;
			this.pswd = pswd;
		}
		public void run(){
			
			String result = "";
			try{
				result = data.start(login, pswd);
			}catch(Exception e){
				e.printStackTrace();
			}
			minus(result);
		}
		
		private  synchronized void plus(){
			numThreads++;
		}
		private  synchronized void minus(String result){
			numThreads--;
			report += result+"\r\n";
			tool.fastWrite(report, "./report");
		}
	}
	
}
