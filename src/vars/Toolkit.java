package vars;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import OptimumWeb.MainToolkit;

public class Toolkit {
static MainToolkit tool = new MainToolkit();
		
	public static void textField(WebDriver driver,  String text, String selector){
		Actions action = new Actions(driver);
		driver.findElement(By.xpath(VAR.convertToXpath(selector))).clear();
		action.sendKeys(driver.findElement(By.xpath(VAR.convertToXpath(selector))), text).perform();
	}
	
	
	public static  void textField(WebElement element,  String text, String selector, WebDriver driver){
		Actions action = new Actions(driver);
		element.findElement(By.xpath(VAR.convertToXpath(selector))).clear();
		action.sendKeys(element.findElement(By.xpath(VAR.convertToXpath(selector))), text).perform();
	}
	
	
	public static  void textField(WebElement element,  String text, WebDriver driver){
		Actions action = new Actions(driver);
		action.sendKeys(element, text).perform();
	}
	
	
	
	public  static void pushButton(WebDriver driver, String selector){
		driver.findElement(By.xpath(VAR.convertToXpath(selector))).click();
	}
	
	
	public static  void pushButton(WebElement element, String selector){
		element.findElement(By.xpath(VAR.convertToXpath(selector))).click();
	}
	
	
	
	public  static void alertAccept(WebDriver driver){
		driver.switchTo().alert().accept();
		tool.sleepS(5);
	}
	
	
	public  static void alertDismiss(WebDriver driver){
		driver.switchTo().alert().dismiss();
		tool.sleepS(10);
	}
	
	
	public  static void pushButtonLink(WebDriver driver, String linktext){
		driver.findElement(By.partialLinkText(linktext)).click();
		tool.sleepS(5);
	}
	
	
	
	
	public static WebElement getEl(WebDriver driver, String xpathElement){
		return driver.findElement(By.xpath(VAR.convertToXpath(xpathElement)));
	}
	public static List<WebElement> getEls(WebDriver driver, String xpathElement){
		return driver.findElements(By.xpath(VAR.convertToXpath(xpathElement)));
	}
	public static WebElement getTag(WebDriver driver, String tagName){
		return driver.findElement(By.tagName(tagName));
	}
	public static List<WebElement> getTags(WebDriver driver, String tagName){
		return driver.findElements(By.tagName(tagName));
	}
	
	
	public static WebElement getEl(WebElement driver, String xpathElement){
		return driver.findElement(By.xpath(VAR.convertToXpath(xpathElement)));
	}
	public static List<WebElement> getEls(WebElement driver, String xpathElement){
		return driver.findElements(By.xpath(VAR.convertToXpath(xpathElement)));
	}
	public static WebElement getTag(WebElement driver, String tagName){
		return driver.findElement(By.tagName(tagName));
	}
	public static List<WebElement> getTags(WebElement driver, String tagName){
		return driver.findElements(By.tagName(tagName));
	}
	
	
	///
	
	public static void login(WebDriver driver){
		driver.get(VAR.rootlink);
		driver.findElement(By.xpath("//input[@id='AdminUsername']")).sendKeys(VAR.pwd);
		driver.findElement(By.xpath("//input[@id='AdminPassword']")).sendKeys(VAR.pwd);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		tool.sleepS(10);
	}
	
	public static void login(WebDriver driver, String login, String pwd){
		
		boolean available = false;
		Actions action = new Actions(driver);
		while(!available){
		try{
			driver.get(VAR.rootlink+"");
			action = new Actions(driver);
			action.sendKeys(driver.findElement(By.xpath("//input[@id='AdminUsername']")), login).perform();
			action.sendKeys(driver.findElement(By.xpath("//input[@id='AdminPassword']")), pwd).perform();
			driver.findElement(By.xpath("//button[@type='submit']")).click();
			tool.sleepS(10);
			available = true;
		}catch(Exception e){
			tool.sleepS(10);
			e.printStackTrace();
		}
		}
	}
	
	
	
	public static  String  navigate(WebDriver driver, int y, String page){
		String out = "";
		
		if(y==1){
			if(driver.getCurrentUrl().equals(VAR.rootlink+"/"+page+"/add"))return VAR.rootlink+"/"+page+"/add";
			
		
			
			driver.get(VAR.rootlink+"/"+page+"/add");
			out =  VAR.rootlink+"/"+page+"/add";
		}
		if(y==0){
			if(driver.getCurrentUrl().equals(VAR.rootlink+"/"+page))return VAR.rootlink+"/"+page;

			
			
			driver.get(VAR.rootlink+"/"+page);
			out =  VAR.rootlink+"/"+page;
		}
		if(y==-1){
			if(driver.getCurrentUrl().equals(VAR.rootlink+"/"+page+"/inactive"))return VAR.rootlink+"/"+page+"/inactive";
			
			
			driver.get(VAR.rootlink+"/"+page+"/inactive");
			out = VAR.rootlink+"/"+page+"/inactive";
		}
		tool.sleepS(5);
		return out;
	}
	
	
	
	public static void dropDown(WebDriver driver, String xpath, String xpath2, String xpath3){
		
		boolean ok = false;
		try{
			WebElement element = driver.findElement(By.xpath(VAR.convertToXpath(xpath)));
			WebElement element2 = element.findElement(By.xpath(VAR.convertToXpath(xpath2)));
			element2.click();
			WebElement element3 = element.findElement(By.xpath(VAR.convertToXpath("ul class='chosen-results'"))).findElement(By.xpath(VAR.convertToXpath(xpath3))); 
			tool.sleepS(1);
			element3.click();
			
			ok = true;
		}catch(Exception e){}	

		
		try{
			if(!ok){
			WebElement element = driver.findElement(By.xpath(VAR.convertToXpath(xpath)));
			WebElement element2 = element.findElement(By.xpath(VAR.convertToXpath(xpath2)));
			element2.click();
			tool.sleepS(15);
			WebElement element3 = element.findElement(By.xpath(VAR.convertToXpath("ul class='chosen-results'"))).findElement(By.xpath(VAR.convertToXpath(xpath3))); 
			tool.sleepS(5);
			
			tool.k(xpath+"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			for(WebElement curent:element.findElements(By.xpath(VAR.convertToXpath("ul class='chosen-results'")))){
				tool.k(curent.getText());
			}
			
			element3.click();
			}
		}catch(Exception e){
			e.printStackTrace();
		}	
		tool.sleepS(1);
		
	}

	
	
	public static void dropDown(WebElement element,  String xpath2, String xpath3){
		boolean ok = false;
		while(!ok){
			try{
//				tool.k("WAIT");
				tool.sleepS(1);
				WebElement element2 = element.findElement(By.xpath(VAR.convertToXpath(xpath2)));
				element2.click();
				ok=true;
			}catch(Exception e){}
		}
		
		try{
			WebElement element3 = element.findElement(By.xpath(VAR.convertToXpath("ul class='chosen-results'"))).findElement(By.xpath(VAR.convertToXpath(xpath3))); 
			tool.sleepS(1);
			element3.click();
		}catch(Exception e){
			e.printStackTrace();
		}	
		tool.sleepS(1);
		
	}
	
	
	public static void dropDown(WebDriver driver, String xpath, String xpath2, String xpath3, int index){
		
		boolean ok = false;
		try{
			WebElement element = driver.findElements(By.xpath(VAR.convertToXpath(xpath))).get(index);
			WebElement element2 = element.findElement(By.xpath(VAR.convertToXpath(xpath2)));
			element2.click();
			ok = true;
		}catch(Exception e){}	

		
		try{
			WebElement element = driver.findElements(By.xpath(VAR.convertToXpath(xpath))).get(index);
			WebElement element2 = element.findElement(By.xpath(VAR.convertToXpath(xpath2)));
			if(!ok)	element2.click();
			WebElement element3 = element.findElement(By.xpath(VAR.convertToXpath("ul class='chosen-results'"))).findElement(By.xpath(VAR.convertToXpath(xpath3))); 
			tool.sleepS(1);
			element3.click();
		}catch(Exception e){
			e.printStackTrace();
		}	
		tool.sleepS(1);
		
	}
	
	
	public static void dropDown(WebElement element0, String xpath, String xpath2, String xpath3, int index){
		
		boolean ok = false;
		try{
			WebElement element = element0.findElements(By.xpath(VAR.convertToXpath(xpath))).get(index);
			WebElement element2 = element.findElement(By.xpath(VAR.convertToXpath(xpath2)));
			element2.click();
			ok = true;
		}catch(Exception e){}	

		
		try{
			WebElement element = element0.findElements(By.xpath(VAR.convertToXpath(xpath))).get(index);
			WebElement element2 = element.findElement(By.xpath(VAR.convertToXpath(xpath2)));
			if(!ok)	element2.click();
			WebElement element3 = element.findElement(By.xpath(VAR.convertToXpath("ul class='chosen-results'"))).findElement(By.xpath(VAR.convertToXpath(xpath3))); 
			tool.sleepS(1);
			element3.click();
		}catch(Exception e){
			e.printStackTrace();
		}	
		tool.sleepS(1);
		
	}
	
	
	public static void activate(WebDriver driver, String expression, int i){
		boolean isVisible = false;
		try{
			driver.findElements(By.xpath("//a[@class='chosen-single']")).get(i).click();
			driver.findElement(By.xpath("//div[@id='"+expression+"']")).findElement(By.xpath("//li[@data-option-array-index='1']")).click();
			isVisible = true;
		}catch(Exception e){
			tool.sleep(300);	
		}	
		if(!isVisible){
			driver.findElements(By.xpath("//a[@class='chosen-single']")).get(i).click();
		
			driver.findElement(By.xpath("//div[@id='"+expression+"']")).findElement(By.xpath("//li[@data-option-array-index='1']")).click();
		}
	}
	
	
	public static List<WebElement> getTR(WebDriver driver, String xpath){
		WebElement bufer = driver.findElement(By.xpath(VAR.convertToXpath(xpath)));
		bufer = bufer.findElement(By.tagName("tbody"));
		return bufer.findElements(By.tagName("tr"));
	}
	
	
	public static WebElement getTD(WebDriver driver, String xpath, int indexTR, int indexTD){
		WebElement bufer = driver.findElement(By.xpath(VAR.convertToXpath(xpath)));
		bufer = bufer.findElement(By.tagName("tbody"));
		return bufer.findElements(By.tagName("tr")).get(indexTR).findElements(By.tagName("td")).get(indexTD);
	}
	
	public static int getPositionOfTableHeader(WebDriver driver, String xpath, String thText){
		int out = -1;
		WebElement bufer = driver.findElement(By.xpath(VAR.convertToXpath(xpath)));
		
		bufer = bufer.findElement(By.tagName("thead"));
		for(WebElement curentTH:bufer.findElements(By.tagName("th"))){
			out++;
			if(curentTH.getText().equals(thText)) break;
		}
		return out;
	}
	
	
}
