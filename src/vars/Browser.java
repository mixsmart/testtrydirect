package vars;

//import java.net.URL;
import java.io.File;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;
//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.remote.RemoteWebDriver;

public class Browser {

	public static WebDriver getDriver(int dimX, int dimY){
		WebDriver driver;
		String gekoPath = Paths.get("./driver").toAbsolutePath().toString();
		new File(gekoPath).setExecutable(true);
		System.setProperty("webdriver.gecko.driver", gekoPath);

		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		
		driver = new FirefoxDriver(capabilities);

		driver.manage().window().setSize(new Dimension(dimX, dimY));
		driver.manage().window().setPosition(new Point(5,5));
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
//		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		
		return driver;
	}
	
	
}
