package vars;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import OptimumWeb.MainToolkit;

public class VAR {
//public static String rootlink = "http://test.try.direct/admin";
//	public static String rootlink = "http://188.138.205.26/admin";
	public static String rootlink = "http://146.185.168.167/admin";
public static String pwd = "testadmin";
public static long sleepTimeS = 3;
public static MainToolkit tool = new MainToolkit();
public static String log = "./logs";


public static void sleep(){
tool.sleepS(sleepTimeS);	
}


public static String getRandomText(){
	tool.sleep(2);
	String out = "";
	out = tool.getMD5(tool.timeStamp()+"");
	return out;
}


public static String getRandomNumber(){
	String out = "";
	
	tool.sleep(1);
	Random r = new Random(System.currentTimeMillis());
	int rand = r.nextInt(100)+200;
	out = rand+"";
	return out;
}



public static String getCurentDate(){
	String out = "";
	
	long time = tool.timeStamp();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	out = dateFormat.format(time);
	
	
	return out;
}



public static String getDateMonthPlus(long x, String del){
	String out = "";
	
	long time = tool.timeStamp();
	time+=(x*30*24*60*60*1000);
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy"+del+"MM"+del+"dd HH:mm");
	out = dateFormat.format(time);
	
	
	return out;
}


public static String convertToXpath(String tag){
	//tag = input id='bla bla'
	//need = //input[@id='bla bla']
	
	String out = "//";
	out+=tag.substring(0, tag.indexOf(" "));
	tag = tag.substring(tag.indexOf(" ")+1);
	out+="[@";
	out+=tag.substring(0, tag.indexOf("="));
	tag = tag.substring(tag.indexOf("=")+1);
	out+="=";
	out+=tag+"]";
	
	
	return out;
}


}
