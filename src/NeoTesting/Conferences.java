package NeoTesting;

import org.openqa.selenium.WebDriver;

import vars.Toolkit;

public class Conferences extends ParlamentCategory{

	 public Conferences(){
		 super.key = "conferences";
	 }

	
	protected boolean testView(WebDriver driver){
		
		try{
			String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, 3).getText();
			
			if(data.equals(firstName)){
				return true;
			}else return false;
		}catch(Exception e) {return false;}
				
	}
	
	
	 protected boolean testModify(WebDriver driver){
		try{
			String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, 3).getText();

			Toolkit.pushButtonLink(driver, data);
			tool.sleepS(30);
		
			addingCore(driver);
			
			Toolkit.activate(driver, Key+"Active_chosen", 0);

			Toolkit.pushButtonLink(driver, "Salveaz");
			Toolkit.alertAccept(driver);
			tool.sleepS(25);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	
		
}
