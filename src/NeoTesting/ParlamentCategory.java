package NeoTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import vars.Browser;
import vars.Toolkit;
import vars.VAR;
import OptimumWeb.MainToolkit;

public class ParlamentCategory {

	protected static MainToolkit tool = new MainToolkit();
	
	 protected String firstName = "";
	 protected String secondName = "";

	 public  String login = "";
	 public  String pwd = "";
	
	 protected String key="";
	 protected String Key = "";
	 protected String keyMod = "";
	 protected int tdLocator = 2;
	
	 protected void keyPrepare(){
		String bufer = key.substring(1, key.length()-1);
		String Bufer = key.substring(0, 1);
		
		Key = Bufer.toUpperCase()+bufer;
		tool.k(Key);
		
//		pagestool.exit();
	}
	

	
	
	public  String start(String login, String pswd){
		keyPrepare();
		String out = "";
		
		WebDriver driver = Browser.getDriver(2000, 1000);

		
		Toolkit.login(driver, login, pswd);
		//==============================================//
		String resource = "";
		
		
		Toolkit.navigate(driver, 0, key);
		eraseBefore(driver);
		
		resource = Toolkit.navigate(driver, 1, key);
		boolean result = testAdd(driver);
		tool.k(result);
		out = resource+" "+getClass().getName()+" Adding: "+result+"\r\n";
		if(result){
			
		resource =Toolkit.navigate(driver, 0, key);
		out+= resource+" "+getClass().getName()+" AddingVerify: "+testView(driver)+"\r\n";
		
		
		out+= resource+" "+getClass().getName()+" CanMakeActive: "+isActive(driver)+"\r\n";
		
//		out+= resource+" CanMakeInactive: "+isInactive(driver);
		
		resource = Toolkit.navigate(driver, 0, key);
		out+=resource+" "+getClass().getName()+" Modify: "+testModify(driver)+"\r\n";
		
		
		resource = Toolkit.navigate(driver, 0, key);
		out+= resource+" "+getClass().getName()+" ModifyVerify: "+testView(driver)+"\r\n";
		
		resource = Toolkit.navigate(driver, 0, key);
		out+=resource+" "+getClass().getName()+" Delete: "+testDelete(driver)+"\r\n";
		}
		driver.quit();
		return out;
	}
	
	
	
	  protected void addingCore(WebDriver driver){
		
		firstName = VAR.getRandomText();
		
		Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
		
		WebElement iframe = driver.findElement(By.xpath("//iframe[@frameborder='0']"));
		driver.switchTo().frame(iframe);
		Toolkit.textField(driver.findElement(By.tagName("body")), VAR.getRandomText(), driver);
		driver.switchTo().defaultContent();
		
		Toolkit.activate(driver, Key+"Active_chosen", 0);

	}
	
	
	  protected  boolean testAdd(WebDriver driver){
//		  if((1-1)==0)return true;
		  
		try{
			
			addingCore(driver);

			sleep(10);
			Toolkit.pushButtonLink(driver, "Salveaz");
			Toolkit.alertAccept(driver);
			sleep(5000);
			try{
				String exampleResponse0 = "alert alert-dismissable alert-success";
				String exampleResponse2 = "alert alert-dismissable alert-warning";
				String exampleResponse1 = "alert alert-dismissable alert-error";
				for(WebElement element:driver.findElements(By.tagName("div"))){
					try{
						String curentResponse = element.getAttribute("class");
					if(curentResponse.equals(exampleResponse0)||curentResponse.equals(exampleResponse2)) return true;
					if(curentResponse.equals(exampleResponse1)) return false;
					
					}catch(Exception e){
					}
				}
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			return true;
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	
	
	  protected boolean testView(WebDriver driver){
		
		try{
			String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, tdLocator).getText();
	
			if(data.equals(firstName)){
				return true;
			}else return false;
		}catch(Exception e) {return false;}
				
	}
	
	
	  protected boolean testModify(WebDriver driver){
		try{
			String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, tdLocator).getText();

			Toolkit.pushButtonLink(driver, data);
			sleep();
		
			addingCore(driver);

//			sleep();
			
			Toolkit.pushButtonLink(driver, "Salveaz");
			Toolkit.alertAccept(driver);
			sleep(5000);
			try{
				String exampleResponse0 = "alert alert-dismissable alert-success";
				String exampleResponse1 = "alert alert-dismissable alert-error";
				for(WebElement element:driver.findElements(By.tagName("div"))){
					try{
						String curentResponse = element.getAttribute("class");
					if(curentResponse.equals(exampleResponse0)) return true;
					if(curentResponse.equals(exampleResponse1)) return false;
					
					}catch(Exception e){
					}
				}
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	
	  protected  boolean testDelete(WebDriver driver){
		
		try{
			
			sleep(0);
			driver.findElement(By.xpath("//input[@type='checkbox']")).click();
			sleep(1000);
			driver.findElement(By.linkText("Mută în coș")).click();
			try{
			driver.switchTo().alert().accept();	
			}catch(Exception e){}
			try{
			sleep(1000);
			driver.findElement(By.xpath("//a[@href='/admin/"+key+"/index/trash']")).click();
			sleep(2000);
			driver.findElement(By.xpath("//input[@type='checkbox']")).click();
			driver.findElement(By.linkText("Ștergere definitivă")).click();
			driver.switchTo().alert().accept();
			sleep(2000);
			}catch(Exception e){}
			return true;
		}catch(Exception e){
			try{
				driver.findElement(By.linkText("Șterge")).click();
				tool.sleepS(2);
				try{
				driver.switchTo().alert().accept();
				}catch(Exception e2){
					e.printStackTrace();
				}
				return true;
			}catch(Exception e2){
				e2.printStackTrace();
				return false;
			}
			
			
		}
		
		
	}
	  
	  
	  protected void eraseBefore(WebDriver driver){
		  try{
			  String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, tdLocator).getText();
			  if(data.length()==64){
				  
				  try{
						sleep(0);
						driver.findElement(By.xpath("//input[@type='checkbox']")).click();
						sleep(0);
						driver.findElement(By.linkText("Mută în coș")).click();
						driver.switchTo().alert().accept();	
						sleep(0); 
					  
				  }catch(Exception e){
					  
						driver.findElement(By.linkText("Șterge")).click();
						sleep(0);
						driver.switchTo().alert().accept();	
					  
				  }
				  
				  
				  
				  
			  }
		  }catch(Exception e){
			  
		  }
	  }
	  
	
	  protected  void sleep(){
		tool.sleepS(25);
	}
	  protected  void sleep(long x){
		tool.sleep(x+10);
	}
	
	  protected boolean isVisibleDelaput(WebDriver driver){
		try{
		Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, 4).findElement(By.tagName("a")).click();
		sleep(20);
		String handleBefore = driver.getWindowHandle();
		
		for(String handle:driver.getWindowHandles()){
			driver.switchTo().window(handle);
		}
		
		
		String data = driver.findElement(By.xpath(VAR.convertToXpath("div class='det_r_nume'"))).getText();

		String fname = data.substring(0, data.indexOf(" "));
		String sname = data.substring(data.indexOf(" ")+1);
		
		driver.close();
		driver.switchTo().window(handleBefore);
		if(fname.equals(firstName)&&sname.equals(secondName.toUpperCase())) return true;
		else return false;
		}catch(Exception e){
			e.printStackTrace();
			return false; 
		}
	}
	  

	
	  protected boolean isActive(WebDriver driver){
		  boolean out = false;
		  
		  try{
		  String isActiveText = "";
		  int indexActivate = Toolkit.getPositionOfTableHeader(driver, "table id='table-"+key+"_admin_index'", "Activ");
		  int indexTitle = Toolkit.getPositionOfTableHeader(driver, "table id='table-"+key+"_admin_index'", "Titlu");
		  String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, indexTitle).getText();
		  if(data.equals(firstName)){
			  isActiveText = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, indexActivate).getText();
			  if(isActiveText.equals("Da"))out = true;
			  else if(isActiveText.equals("Nu"))out = false;
			else
				tool.k("A column \"Activ\" not found!");
		  }
		  
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  return out;
	  }
	  
	  
	  
	  protected boolean isInactive(WebDriver driver){
		  boolean out = false;
		  
		  try{
		  String isActiveText = "";
		  int indexActivate = Toolkit.getPositionOfTableHeader(driver, "table id='table-"+key+"_admin_index'", "Activ");
		  int indexTitle = Toolkit.getPositionOfTableHeader(driver, "table id='table-"+key+"_admin_index'", "Titlu");
		  Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, 0).click();
		  driver.findElement(By.partialLinkText("nepublica")).click();
		  String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, indexTitle).getText();
		  if(data.equals(firstName)){
			  isActiveText = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, indexActivate).getText();
			  if(isActiveText.equals("Nu"))out = true;
			  else if(isActiveText.equals("Da"))out = false;
			else{
				tool.k("A column \"Activ\" not found!");
			}
		  }
		  
		  }catch(Exception e){
			  e.printStackTrace();
		  }

		  return out;
	  }

	  
		public void scroll(WebDriver driver, WebElement webElement, int x, int y) {
			 
		    String code = "window.scroll(" + (webElement.getLocation().x + x) + ","
		                                 + (webElement.getLocation().y + y) + ");";
		 
		    ((JavascriptExecutor)driver).executeScript(code, webElement, x, y);
		 
		}
		
		public void scroll(WebDriver driver, WebElement webElement){
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();"
                    ,webElement);
		}
	  
		public void zoom(WebDriver driver, WebElement element){
			((JavascriptExecutor)driver).executeScript("document.body.style.MozTransform = \"scale(0.25)\"", element);
		}
		
		public static void setAttribute(WebDriver driver, WebElement element, String attribute, String value) {
			((JavascriptExecutor) driver).executeScript("arguments[0].setAttribute('" + attribute
			+ "',arguments[1]);",	element,	value);
			}
}
