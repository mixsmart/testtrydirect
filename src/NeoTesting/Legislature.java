package NeoTesting;

import org.openqa.selenium.WebDriver;

import vars.Toolkit;
import vars.VAR;

public class Legislature extends ParlamentCategory{
	
	public Legislature(){
		super.key = "legislatures";
	}
	

	
	protected boolean testAdd(WebDriver driver){

		try{
			
			firstName = VAR.getRandomText();
			
			Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
			
			Toolkit.textField(driver, VAR.getDateMonthPlus(365, "/"), "input id='"+Key+"StartYear'");
			
			Toolkit.textField(driver, VAR.getDateMonthPlus(368, "/"), "input id='"+Key+"EndYear'");

			Toolkit.activate(driver, Key+"Active_chosen", 0);
	
			Toolkit.pushButtonLink(driver, "Salveaz");
			sleep();
			Toolkit.alertAccept(driver);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	
	
	
	 protected boolean testModify(WebDriver driver){
		try{
			String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, 2).getText();

			Toolkit.pushButtonLink(driver, data);
			tool.sleepS(30);
		
			
			firstName = VAR.getRandomText();
			Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
			
			Toolkit.textField(driver, VAR.getDateMonthPlus(465, "/"), "input id='"+Key+"StartYear'");
			
			Toolkit.textField(driver, VAR.getDateMonthPlus(468, "/"), "input id='"+Key+"EndYear'");

			
			Toolkit.activate(driver, Key+"Active_chosen", 0);

			Toolkit.pushButtonLink(driver, "Salveaz");
			Toolkit.alertAccept(driver);
			tool.sleepS(25);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}

	
	
}
