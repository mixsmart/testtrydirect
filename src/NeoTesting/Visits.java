package NeoTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import vars.Browser;
import vars.Toolkit;
import vars.VAR;

public class Visits extends ParlamentCategory{

	
	 public Visits(){
		 super.key = "visits";
	 }
	 
	 
	 
		public  String start(String login, String pswd){
			keyPrepare();
			String out = "";
			String resource = "";
			WebDriver driver = Browser.getDriver(2000, 1000);
			Toolkit.login(driver, login, pswd);
			//==============================================//
			
			resource = Toolkit.navigate(driver, 1, key);
			boolean result = testAdd(driver);
			out = resource+" "+getClass().getName()+" Adding: "+result+"\r\n";
			
			if(result){
			
			resource = Toolkit.navigate(driver, 0, key);
			sort(driver);	
			out+= resource+" "+getClass().getName()+" AddingVerify: "+testView(driver)+"\r\n";
	
			out+= resource+" "+getClass().getName()+" CanMakeActive: "+isActive(driver)+"\r\n";
			
			resource = Toolkit.navigate(driver, 0, key);
			out+=resource+" "+getClass().getName()+" Modify: "+testModify(driver)+"\r\n";
			
			
			resource = Toolkit.navigate(driver, 0, key);
			out+= resource+" "+getClass().getName()+" ModifyVerify: "+testView(driver)+"\r\n";

			
			
			resource = Toolkit.navigate(driver, 0, key);
			out+=resource+" "+getClass().getName()+" Delete: "+testDelete(driver)+"\r\n";
	 
			}
			driver.quit();
			return out;
		}
		
		
		
		public void sort(WebDriver driver){
			for(int i=0; i<2; i++){
				
				driver.findElement(By.xpath(VAR.convertToXpath("th data-name='id'"))).click();
				tool.sleepS(3);
				
				
			}
			
			
			
			
		}
	 
}
