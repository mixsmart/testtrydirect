package NeoTesting;

import org.openqa.selenium.WebDriver;

import vars.Toolkit;
import vars.VAR;

public class Transcripts extends ParlamentCategory{


	public Transcripts(){
		super.key = "transcripts";
	}
	
	
	
	protected void addingCore(WebDriver driver){
		firstName = VAR.getRandomText();
		Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
	}
	
	
	
	
	protected boolean testAdd(WebDriver driver){
		try{
			addingCore(driver);	
		Toolkit.activate(driver, Key+"Active_chosen", 0);
		Toolkit.dropDown(driver, "div id='Document_chosen'", "a class='chosen-single chosen-default'", "li data-option-array-index='2'");
		tool.sleepS(15);
		Toolkit.pushButtonLink(driver, "Salveaz");
		Toolkit.alertAccept(driver);
		return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	
	
	
	 protected boolean testModify(WebDriver driver){
		try{
			String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, 2).getText();
			Toolkit.pushButtonLink(driver, data);
			tool.sleepS(30);
			addingCore(driver);
			Toolkit.activate(driver, Key+"Active_chosen", 0);
			Toolkit.dropDown(driver, "div id='Document_chosen'", "a class='chosen-single chosen-default'", "li data-option-array-index='2'");
			tool.sleepS(15);
	Toolkit.pushButtonLink(driver, "Salveaz");
	Toolkit.alertAccept(driver);
	tool.sleepS(25);
	return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
}
