package NeoTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import vars.Browser;
import vars.Toolkit;
import vars.VAR;

public class CoreDeput extends ParlamentCategory{


	public CoreDeput(){
		super.key = "users";
	}
	
	public String start(String login, String pswd){
		keyPrepare();
		String out = "";
		String resource = "";
		WebDriver driver = Browser.getDriver(2000, 1000);
		Toolkit.login(driver, login, pswd);
		//==============================================//
		
		resource = Toolkit.navigate(driver, 1, key);
		
		boolean result = testAdd(driver);
				
		out = resource+" "+getClass().getName()+" Adding: "+result+"\r\n";
		
		if(result){
			resource = Toolkit.navigate(driver, 0, key);
			out+= resource+" "+getClass().getName()+" AddingVerify: "+testView(driver)+"\r\n";
			
			out+= resource+" "+getClass().getName()+" CanMakeActive: "+isActive(driver)+"\r\n";
			
			resource = Toolkit.navigate(driver, 0, key);
			out+=resource+" "+getClass().getName()+" Modify: "+testModify(driver)+"\r\n";
			
			
			resource = Toolkit.navigate(driver, 0, key);
			out+= resource+" "+getClass().getName()+" ModifyVerify: "+testView(driver)+"\r\n";
			
			resource = Toolkit.navigate(driver, 0, key);
			out += resource+" "+getClass().getName()+" DelaputIsVisible: "+isVisibleDelaput(driver)+"\r\n";
			
			resource = Toolkit.navigate(driver, 0, key);
			out+=resource+" "+getClass().getName()+" Delete: "+testDelete(driver)+"\r\n";
		}
		driver.quit();
		return out;
	}
	
	

	
	
	protected boolean testAdd(WebDriver driver){

		try{
			
			firstName = VAR.getRandomText();
		Toolkit.textField(driver, firstName, "input id='"+Key+"FirstName'");
			secondName = VAR.getRandomText();
		Toolkit.textField(driver, secondName, "input id='"+Key+"LastName'");
		
		Toolkit.textField(driver, VAR.getRandomText()+"@"+VAR.getRandomText()+".com", "input id='"+Key+"Email'");
		String pswd = VAR.getRandomText();
		Toolkit.textField(driver, pswd, "input id='"+Key+"Password'");
		Toolkit.textField(driver, pswd, "input id='"+Key+"ConfPassword'");

		
		Toolkit.activate(driver, ""+Key+"Active_chosen", 1);

		Toolkit.pushButtonLink(driver, "Salveaz");
		Toolkit.alertAccept(driver);
		
		
		
		try{
			String exampleResponse0 = "alert alert-dismissable alert-success";
			String exampleResponse2 = "alert alert-dismissable alert-warning";
			String exampleResponse1 = "alert alert-dismissable alert-error";
			for(WebElement element:driver.findElements(By.tagName("div"))){
				try{
					String curentResponse = element.getAttribute("class");
				if(curentResponse.equals(exampleResponse0)||curentResponse.equals(exampleResponse2)) return true;
				if(curentResponse.equals(exampleResponse1)) return false;
				
				}catch(Exception e){
				}
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	
	
	 protected boolean testView(WebDriver driver){
		
		try{
		String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, 2).getText();
		
		String fname = data.substring(0, data.indexOf(" "));
		String sname = data.substring(data.indexOf(" ")+1);
		
		if(fname.equals(firstName)&&sname.equals(secondName.toUpperCase())){
			return true;
		}else return false;
		}catch(Exception e) {return false;}
				
	}
	
	
	protected boolean testModify(WebDriver driver){
		try{
			String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, 2).getText();
			
			String fname = data.substring(0, data.indexOf(" "));
			Toolkit.pushButtonLink(driver, fname);
			tool.sleepS(45);
		
		firstName = VAR.getRandomText();
	Toolkit.textField(driver, firstName, "input id='"+Key+"FirstName'");
		secondName = VAR.getRandomText();
	Toolkit.textField(driver, secondName, "input id='"+Key+"LastName'");
	
	String email =  VAR.getRandomText()+"@"+VAR.getRandomText()+".com";
	login = email;
	Toolkit.textField(driver, email, "input id='"+Key+"Email'");
	String pswd = VAR.getRandomText();
	pwd = pswd;
	Toolkit.textField(driver, pswd, "input id='"+Key+"Password'");
	Toolkit.textField(driver, pswd, "input id='"+Key+"ConfPassword'");

	
	Toolkit.activate(driver, ""+Key+"Active_chosen", 1);

	Toolkit.pushButtonLink(driver, "Salveaz");
	Toolkit.alertAccept(driver);
	tool.sleepS(25);
	
	try{
		String exampleResponse0 = "alert alert-dismissable alert-success";
		String exampleResponse2 = "alert alert-dismissable alert-warning";
		String exampleResponse1 = "alert alert-dismissable alert-error";
		for(WebElement element:driver.findElements(By.tagName("div"))){
			try{
				String curentResponse = element.getAttribute("class");
			if(curentResponse.equals(exampleResponse0)||curentResponse.equals(exampleResponse2)) return true;
			if(curentResponse.equals(exampleResponse1)) return false;
			
			}catch(Exception e){
			}
		}
		
		
	}catch(Exception e){
		e.printStackTrace();
	}
	
	return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	


	  protected boolean isActive(WebDriver driver){
		  boolean out = false;
		  String isActiveText = "";
		  
		  int indexActivate = Toolkit.getPositionOfTableHeader(driver, "table id='table-"+key+"_admin_index'", "Activ");
		  int indexTitle = Toolkit.getPositionOfTableHeader(driver, "table id='table-"+key+"_admin_index'", "Nume");
		  
		  String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, indexTitle).getText();
		  if(data.equals(firstName)){
			  isActiveText = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, indexActivate).getText();
			  if(isActiveText.equals("Da"))out = true;
			  else if(isActiveText.equals("Nu"))out = false;
			else
				try {
					throw new Exception("A column \"Activ\" not found!");
				} catch (Exception e) {
					e.printStackTrace();
				}
		  }
		  return out;
	  }

	  
	  protected boolean isInactive(WebDriver driver){
		  boolean out = false;
		  String isActiveText = "";
		  int indexActivate = Toolkit.getPositionOfTableHeader(driver, "table id='table-"+key+"_admin_index'", "Activ");
		  int indexTitle = Toolkit.getPositionOfTableHeader(driver, "table id='table-"+key+"_admin_index'", "Nume");
		  Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, 0).click();
		  driver.findElement(By.partialLinkText("nepublica")).click();
		  String data = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, indexTitle).getText();
		  if(data.equals(firstName)){
			  isActiveText = Toolkit.getTD(driver, "table id='table-"+key+"_admin_index'", 0, indexActivate).getText();
			  if(isActiveText.equals("Nu"))out = true;
			  else if(isActiveText.equals("Da"))out = false;
			else
				try {
					throw new Exception("A column \"Activ\" not found!");
				} catch (Exception e) {
					e.printStackTrace();
				}
		  }
		  return out;
	  }
	  
	  
	
	 
	
}




