package Proiecte_Legislative;

import org.openqa.selenium.WebDriver;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class PublicAuthorities extends ParlamentCategory{
	
	public PublicAuthorities(){
		super.key = "public_authorities";
	}
	
	
	protected void addingCore(WebDriver driver){
		firstName = VAR.getRandomText();
		Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
		Toolkit.activate(driver, Key+"Active_chosen", 0);

	}
	  
	  
	protected void keyPrepare(){
			 	Key = "PublicAuthority";
				tool.k(Key);
	}

}
