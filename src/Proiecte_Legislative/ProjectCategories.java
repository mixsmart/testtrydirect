package Proiecte_Legislative;


import org.openqa.selenium.WebDriver;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class ProjectCategories extends ParlamentCategory{
	public ProjectCategories(){
		super.key = "project_categories";
		super.tdLocator = 1;
	}
	
	
	protected void addingCore(WebDriver driver){
		firstName = VAR.getRandomText();
		Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
	}
	  
	  
	protected void keyPrepare(){
			 	Key = "ProjectCategory";
				tool.k(Key);
	}
	  
}
