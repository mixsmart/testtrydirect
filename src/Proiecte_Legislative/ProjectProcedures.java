package Proiecte_Legislative;

import org.openqa.selenium.WebDriver;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class ProjectProcedures extends ParlamentCategory{

	public ProjectProcedures(){
		super.key = "project_procedures";
		super.tdLocator = 1;
	}
	
	
	protected void addingCore(WebDriver driver){
		firstName = VAR.getRandomText();
		Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
	}
	  
	  
	protected void keyPrepare(){
			 	Key = "ProjectProcedure";
				tool.k(Key);
	}
}
