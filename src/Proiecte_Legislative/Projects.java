package Proiecte_Legislative;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import vars.Toolkit;
import vars.VAR;
import NeoTesting.ParlamentCategory;

public class Projects extends ParlamentCategory{

	public Projects(){
		super.key = "projects";
		super.tdLocator = 3;
	}
	
	
	  protected void addingCore(WebDriver driver){		
		firstName = VAR.getRandomText();
		
		
		
		ArrayList<WebElement> buffer = new ArrayList<>();
		

		buffer.add(driver.findElement(By.xpath(VAR.convertToXpath("div id='ProjectRegistrationStage_chosen'"))));
		buffer.add(driver.findElement(By.xpath(VAR.convertToXpath("div id='ProjectProjectCategoryId_chosen'"))));
		buffer.add(driver.findElement(By.xpath(VAR.convertToXpath("div id='ProjectLegislatureId_chosen'"))));
		

		Toolkit.textField(driver, firstName, "input id='"+Key+"TitleRum'");
		String textFill = VAR.getRandomNumber();
		Toolkit.textField(driver, textFill, "input id='ProjectRegistrationNumber'");
		textFill = VAR.getCurentDate();
		Toolkit.textField(driver, textFill, "input id='ProjectRegistrationDate'");
		//только в таком порядке работает, и я хз что за чудеса... не моя вина
		Toolkit.activate(driver, Key+"Active_chosen", 0);
		
		Toolkit.dropDown(buffer.get(0),  "div id='ProjectRegistrationStage_chosen'", "li data-option-array-index='1'");
		
		Toolkit.dropDown(buffer.get(1),  "div id='ProjectProjectCategoryId_chosen'", "li data-option-array-index='1'");
		Toolkit.dropDown(buffer.get(1),  "div id='ProjectProjectCategoryId_chosen'", "li data-option-array-index='1'");
		
		Toolkit.dropDown(buffer.get(2), "div id='ProjectLegislatureId_chosen'", "li data-option-array-index='1'");
		Toolkit.dropDown(buffer.get(2), "div id='ProjectLegislatureId_chosen'", "li data-option-array-index='1'");
	}
	

}
